package ru.hldns.klaw.web.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.context.MessageSource;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import ru.hldns.klaw.data.model.User;
import ru.hldns.klaw.service.UserService;
import ru.hldns.klaw.web.errors.NotFoundUserException;

import java.util.Optional;

import static org.mockito.Mockito.*;

class UserSettingsPageServiceTest {
    private UserService service;
    private MessageSource messageSource;
    private UserSettingsPageService userSettingsPageService;

    @BeforeEach
    void setUp() {
        service = mock(UserService.class);
        messageSource = mock(MessageSource.class);
        userSettingsPageService = new UserSettingsPageService(service, messageSource);
    }

    @Test
    void changeUserPassword() throws NotFoundUserException {
        User user = new User();
        user.setUsername("user");
        when(service.loadByName("user")).thenReturn(Optional.of(user));
        when(service.validatePassword(user, "current")).thenReturn(true);

        userSettingsPageService.changeUserPassword(
                "user", "current", "new",
                Mockito.mock(BindingResult.class), Mockito.mock(Model.class));
        verify(service, times(1)).validatePassword(user, "current");
        verify(service, times(1)).changePassword(user, "new");
    }

    @Test
    void changeUserPasswordWithWrongCurrentPassword() throws NotFoundUserException {
        var user = new User();
        var bindingResult = mock(BindingResult.class);
        when(service.loadByName("user")).thenReturn(Optional.of(user));
        when(service.validatePassword(user, "current")).thenReturn(false);

        userSettingsPageService.changeUserPassword(
                "user", "current", "new",
                bindingResult, Mockito.mock(Model.class));

        verify(service, times(1)).validatePassword(user, "current");
        verify(bindingResult, times(1)).addError(any());
    }
}