package ru.hldns.klaw.web.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import ru.hldns.klaw.web.service.UserSettingsPageService;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = UserSettingsController.class)
class UserSettingsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserSettingsPageService service;

    @Test
    @WithMockUser
    void getSettings() throws Exception {
        mockMvc.perform(get("/settings"))
                .andExpect(status().isOk());
    }

    @Test
    void getSettingsWithoutAuthorization() throws Exception {
        mockMvc.perform(get("/settings"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser(username = "Author")
    void changeUserPassword() throws Exception {
        mockMvc.perform(
                post("/settings")
                        .param("currentPassword", "current")
                        .param("newPassword", "newPass")
                        .param("confirmation", "newPass"))
                .andExpect(status().isOk());
        verify(service, times(1)).changeUserPassword(eq("Author"), eq("current"), eq("newPass"), any(), any());
    }
}