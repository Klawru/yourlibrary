package ru.hldns.klaw.utility;

import org.junit.Assert;

import org.junit.jupiter.api.Test;

public class CommonUtilityTest {

    @Test
    public void getPathFromMD5() {
        Assert.assertEquals("/b1/53/c234d135622bd7ebe4ff0d4f6263.jpeg", CommonUtility.getPathFromMD5("b153c234d135622bd7ebe4ff0d4f6263", "jpeg"));
        Assert.assertEquals("/fb/3d/de6d10f7d0ea164b4d87a2b0eaf8.png", CommonUtility.getPathFromMD5("fb3dde6d10f7d0ea164b4d87a2b0eaf8","png"));
    }

    @Test
    public void getExtension() {
        Assert.assertEquals("jpeg", CommonUtility.getExtension("image/jpeg"));
        Assert.assertEquals("png", CommonUtility.getExtension("image/png"));
    }

}