package ru.hldns.klaw.utility;

import org.junit.Assert;
import org.junit.Before;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;

public class PaginationTest {

    @SuppressWarnings("unchecked")
    private Page<Object> getPage(int currentPage, int TotalPage){
        Page<Object> page = (Page<Object>) Mockito.mock(Page.class);
        Mockito.when(page.getNumber()).thenReturn(currentPage-1);
        Mockito.when(page.getTotalPages()).thenReturn(TotalPage);
        return page;
    }

    @Test
    public void constructorTest() throws Exception {
        final var pagination = Pagination.of(6,120,20,3);
        Assert.assertEquals(6,pagination.getNow());
        Assert.assertEquals(6,pagination.getLastPage());
        Assert.assertFalse(pagination.hasNext());
        Assert.assertTrue(pagination.hasPrevious());
        Assert.assertArrayEquals(new int[]{},pagination.next);
        Assert.assertArrayEquals(new int[]{3,4,5},pagination.previous);
    }

    @Test
    public void constructorTest3() throws Exception {
        final var pagination = Pagination.of(1,5,20,3);
        Assert.assertEquals(1,pagination.getNow());
        Assert.assertEquals(1,pagination.getLastPage());
        Assert.assertFalse(pagination.hasNext());
        Assert.assertFalse(pagination.hasPrevious());
        Assert.assertArrayEquals(new int[]{},pagination.next);
        Assert.assertArrayEquals(new int[]{},pagination.previous);
    }

    @Test
    public void constructorTest2() throws Exception {
        final var pagination = Pagination.of(5,100,10,3);
        Assert.assertEquals(5,pagination.getNow());
        Assert.assertEquals(10,pagination.getLastPage());
        Assert.assertTrue(pagination.hasNext());
        Assert.assertTrue(pagination.hasPrevious());
        Assert.assertArrayEquals(new int[]{6,7,8},pagination.next);
        Assert.assertArrayEquals(new int[]{2,3,4},pagination.previous);
    }

    @Test
    public void constructorPage() throws Exception {
        final var pagination = Pagination.of(getPage(10, 20), 3);
        Assert.assertEquals(10,pagination.getNow());
        Assert.assertEquals(20,pagination.getLastPage());
        Assert.assertTrue(pagination.hasNext());
        Assert.assertTrue(pagination.hasPrevious());
    }

    @Test
    public void constructorTestArrays1() throws Exception {
        final var pagination = Pagination.of(getPage(1, 5), 3);
        Assert.assertArrayEquals(new int[]{},pagination.previous);
        Assert.assertArrayEquals(new int[]{2,3,4},pagination.next);
    }

    @Test
    public void constructorTestArrays2() throws Exception {
        final var pagination = Pagination.of(getPage(3, 5), 3);
        Assert.assertArrayEquals(new int[]{1,2},pagination.previous);
        Assert.assertArrayEquals(new int[]{4,5},pagination.next);
    }

    @Test
    public void constructorTestArrays3() throws Exception {
        final var pagination = Pagination.of(getPage(10, 55), 2);
        Assert.assertArrayEquals(new int[]{8,9},pagination.previous);
        Assert.assertArrayEquals(new int[]{11,12},pagination.next);
    }

    @Test
    public void constructorTestArrays4() throws Exception {
        final var pagination = Pagination.of(getPage(10, 11), 3);
        Assert.assertArrayEquals(new int[]{7,8,9},pagination.previous);
        Assert.assertArrayEquals(new int[]{11},pagination.next);
    }
    
    @Test
    public void constructorTestArrays5() throws Exception {
        final var pagination = Pagination.of(getPage(20, 20), 3);
        Assert.assertArrayEquals(new int[]{17,18,19},pagination.previous);
        Assert.assertArrayEquals(new int[]{},pagination.next);
    }
}