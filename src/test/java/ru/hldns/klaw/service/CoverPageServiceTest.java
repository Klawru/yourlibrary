package ru.hldns.klaw.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mockito;
import org.springframework.util.FastByteArrayOutputStream;
import ru.hldns.klaw.config.ApplicationConfig;
import ru.hldns.klaw.utility.Either;
import ru.hldns.klaw.web.service.CoverPageService;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import static org.junit.Assert.assertTrue;

public class CoverPageServiceTest {

    @TempDir
    public Path imgFolder;
    @TempDir
    public Path thbFolder;

    private ApplicationConfig applicationConfig;
    private CoverPageService coverPageService;

    @BeforeEach
    public void setUp() {
        applicationConfig = Mockito.mock(ApplicationConfig.class);
        Mockito.when(applicationConfig.getImgUploadDir()).thenReturn(imgFolder.toString());
        Mockito.when(applicationConfig.getThumbnailDir()).thenReturn(thbFolder.toString());

        coverPageService = new CoverPageService(applicationConfig);
    }

    @Test
    public void save() throws IOException {
        final byte[] bytes = getImage();
        final Either<String, Exception> path = coverPageService.save(bytes, "b153c234d135622bd7ebe4ff0d4f6263");
        assertTrue(path.isPresent());
        final var savedImgFile = new File(imgFolder + path.getObject());
        final var savedThumbnailFile = new File(imgFolder + path.getObject());
        assertTrue(savedImgFile.exists());
        assertTrue(savedThumbnailFile.exists());
        assertTrue(savedImgFile.length()>0);
        assertTrue(savedThumbnailFile.length()>0);
    }

    private byte[] getImage() throws IOException {
        BufferedImage image = new BufferedImage(500, 500, BufferedImage.TYPE_INT_RGB);
        FastByteArrayOutputStream outputStream = new FastByteArrayOutputStream();
        ImageIO.write(image, "jpg", outputStream);
        return outputStream.toByteArray();
    }
}