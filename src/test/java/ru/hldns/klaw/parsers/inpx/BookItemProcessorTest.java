package ru.hldns.klaw.parsers.inpx;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.context.MessageSource;
import ru.hldns.klaw.data.model.Author;
import ru.hldns.klaw.data.model.Book;
import ru.hldns.klaw.data.model.Genre;

import java.time.LocalDate;
import java.util.Locale;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasKey;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class BookItemProcessorTest {
    BookItemProcessor processor;

    @BeforeEach
    public void setUp() {
        MessageSource messageSource = Mockito.mock(MessageSource.class);
        Mockito.when(messageSource.getMessage("author.unknown", null, Locale.getDefault())).thenReturn("unknown");
        Mockito.when(messageSource.getMessage("untitled", null, Locale.getDefault())).thenReturn("unknown");
        processor = new BookItemProcessor(messageSource);
    }

    @Test
    public void process() {
        final Book book = processor.process("LastName,FirstName,MiddleName:LastName,F,M\u0004genre:genre2:\u0004Title\u0004Series\u00041\u0004file\u0004777\u0004666\u00040\u0004ext\u00042019-12-12\u0004ru \u00045\u0004Key,Word\u0004folder");
        assertNotNull(book);
        assertThat(book.getAuthors(), hasItem(Author.of("LastName", "FirstName", "MiddleName")));
        assertThat(book.getGenres(), hasItem(Genre.of("genre")));
        assertThat(book.getGenres(), hasItem(Genre.of("genre2")));
        assertThat(book.getTitle(), is("Title"));
        assertThat(book.getSeries().getName(), is("Series"));
        assertThat(book.getSeriesNumber(), equalTo(1));
        assertThat(book.getFile(), equalTo("file"));
        assertThat(book.getFileSize(), equalTo(777));
        assertThat(book.getLibId(), equalTo(666));
        assertThat(book.isDeleted(), is(false));
        assertThat(book.getFileExtension(), equalTo("ext"));
        assertThat(book.getDate(), equalTo(LocalDate.parse("2019-12-12")));
        assertThat(book.getLang(), is("ru"));
        assertThat(book.getProperties(), hasKey("keyword"));
        assertThat(book.getFolder(), is("folder"));
    }
}