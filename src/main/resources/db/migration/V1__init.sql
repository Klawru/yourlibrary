CREATE EXTENSION IF NOT EXISTS unaccent;

CREATE OR REPLACE FUNCTION f_unaccent(text varchar) RETURNS varchar AS
$func$
SELECT unaccent('unaccent', lower($1)) -- schema-qualify function and dictionary --
$func$ LANGUAGE sql IMMUTABLE;

--Author Table
create sequence author_seq increment by 10;
create table authors
(
    id_author   integer      not null primary key,
    first_name  varchar(128),
    middle_name varchar(128),
    last_name   varchar(128) not null,
    full_name   varchar(400) unique
);
create index idx_full_name_unaccent on authors (f_unaccent(full_name));

--Community Table
create table community
(
    id_community serial not null primary key,
    name         varchar(150)
);

--Genres Table
create table genres
(
    id_genre     serial       not null
        constraint genres_pkey primary key,
    id_community integer      not null
        constraint genres_ref_community references community,
    genre        varchar(150) not null
        constraint genres_iniq unique,
    name         varchar(150) not null,
    abstract     varchar(500)
);

--Series Table
create sequence series_seq increment by 10;
create table series
(
    id_series integer not null
        constraint series_pkey primary key,
    name      varchar(250)
        constraint series_uniq_name unique
);
create index idx_name_unaccent on series (f_unaccent(name));

--Book Table
create sequence book_seq increment by 10;
create table books
(
    id_book          integer      not null
        constraint books_pkey primary key,
    lib_id           integer      not null,
    series_id_series integer
        constraint books_ref_series references series,
    series_number    integer,
    file_size        integer,
    published_date   smallint,
    deleted          boolean,
    date             date,
    name             varchar(250) not null,
    publisher        varchar(255),
    file_size_string varchar(10),
    file             varchar(255),
    file_extension   varchar(4),
    cover_page       varchar(40),
    folder           varchar(500),
    lang             varchar(4),
    annotation       text,
    properties       jsonb,
    description      jsonb
);

--books2authors Table
create table books2authors
(
    id_book   integer not null
        constraint books2authors_ref_books references books,
    id_author integer not null
        constraint books2authors_ref_authors references authors,
    constraint books2authors_pkey primary key (id_book, id_author)
);
create index author_index on books2authors (id_author);

--genres2books Table
create table genres2books
(
    id_book  integer not null
        constraint genres2books_ref_books references books,
    id_genre integer not null
        constraint genres2books_ref_genres references genres,
    constraint genres2books_pkey primary key (id_book, id_genre)
);
create index genres_index on genres2books (id_genre);

--SeriesAuthor View
CREATE MATERIALIZED VIEW IF NOT EXISTS series_author as
SELECT distinct series.*, a.id_author
FROM series
         JOIN books b on series.id_series = b.series_id_series
         JOIN books2authors b2a on b.id_book = b2a.id_book
         JOIN authors a on b2a.id_author = a.id_author;
CREATE INDEX series_author_fname_ind ON series_author (id_author);

--Person Table
create sequence eperson_seq;

create table eperson
(
    eperson_id              integer      not null
        constraint eperson_pkey primary key,
    non_expired             boolean,
    non_locked              boolean,
    can_log_in              boolean,
    credentials_non_expired boolean,
    name                    varchar(20)  not null
        constraint eperson_uniq_name unique,
    password                varchar(60)  not null,
    email                   varchar(254) not null
        constraint eperson_uniq_email unique
);

--Person Group Table
create table epersongroup
(
    eperson_group_id uuid         not null
        constraint epersongroup_pkey primary key,
    name             varchar(255) not null
);

--epersongroup2person Table
create table epersongroup2person
(
    eperson_id      integer not null
        constraint epg2p_ref_eperson references eperson,
    epersongroup_id uuid    not null
        constraint epg2p_ref_epersongroup references epersongroup,
    constraint epersongroup2person_pkey primary key (eperson_id, epersongroup_id)
);

--Parsed File Table
create table parsed_file
(
    hash        bigint       not null,
    rows        bigint       not null,
    is_complete boolean      not null,
    name        varchar(255) not null
        constraint parsed_file_pkey primary key
);