--Last update table
create table last_update_table
(
    book_count bigint       not null,
    updated    timestamp    not null,
    name       varchar(255) not null
        constraint last_update_name_pkey primary key
);