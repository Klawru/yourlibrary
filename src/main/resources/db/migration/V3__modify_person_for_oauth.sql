alter table eperson
    add attributes jsonb,
    add oauth      boolean not null default false,
    alter column password drop not null;

alter table epersongroup
    add constraint epersongroup_name_unique unique (name);

INSERT into epersongroup
values ('c4cb50c3-4915-4328-b1d8-198d5621f513', 'CAN_DOWNLOAD')
ON CONFLICT DO NOTHING;

INSERT into epersongroup2person
SELECT p.eperson_id, 'c4cb50c3-4915-4328-b1d8-198d5621f513'
from eperson p
ON CONFLICT DO NOTHING;