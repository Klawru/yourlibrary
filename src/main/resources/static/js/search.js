//Фильтр по умолчанию убираем со страницы

class search {

    constructor() {
        this.selectsType = new Array(5);
        this.selectsOper = new Array(5);
        this.formInputs = new Array(5);
        this.filters = $('#filterForm .row');
        this.communityFilter = $('#communityFilter');
        $('#formAddFilter').on('click', (e) => this.addForm(e));
        $('#authorsFacet').on('click', 'button', (e) => this.addAuthorFilterToForm(e));
        $('#genreFacet').on('click', 'button', (e) => this.addGenreFilterToForm(e));
        $('#filterForm').on('click', '.btn', (e) => this.removeFilterValue(e));
        this.init()
    }

    init() {
        let first = true;
        for (let i = 0; i < this.filters.length; i++) {
            this.selectsType[i] = this.filters.find('#form_filter_type' + i)[0];
            this.selectsOper[i] = this.filters.find('#form_filter_operation' + i)[0];
            this.formInputs[i] = this.filters.find('#form_filter_input' + i)[0];
            if (this.formInputs[i].value === "") {
                if (first === true)
                    first = false;
                else
                    this.filters[i].hidden = 'hidden';
            }

            $('#removeFormFilter_' + i).on("click", (e) => this.removeFilterValue(e))
        }
    }

    addForm(event) {
        for (let i = 0; i < this.filters.length; i++) {
            if (this.filters[i].hidden === true) {
                this.filters[i].removeAttribute('hidden');
                event.preventDefault();
                break;
            }
        }

    }

    addAuthorFilterToForm(event) {
        for (let i = 0; i < 5; i++) {
            if (this.formInputs[i].value === "") {
                this.selectsType[i].value = 'author';
                this.selectsOper[i].value = 'equals';
                this.formInputs[i].value = event.currentTarget.value;
                if (this.filters[i].hidden === true) {
                    this.filters[i].removeAttribute('hidden');
                }
                search.formSubmit();
                break;
            }
        }
        event.preventDefault();
    }

    addGenreFilterToForm(event) {
        this.communityFilter.val(event.currentTarget.value);
        search.formSubmit();
        event.preventDefault();
    }

    removeFilterValue(event) {
        let target = event.currentTarget;
        let i = parseInt(target.attributes["value"].textContent);
        if (this.formInputs[i].value !== "") {
            this.formInputs[i].value = ""
        } else if (target.hidden !== true) {
            this.hide(i);
        }
        event.preventDefault()
    }

    static formSubmit() {
        $('#searchFrom').submit();
    }

    hide(i) {
        this.filters[i].hidden = 'hidden'
    }
}

let searchPage;
$(document).ready(function () {
    searchPage = new search()
});



