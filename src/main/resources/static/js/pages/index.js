let ctx = document.getElementById("genreChart");
let genreChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: countLabel,
        datasets: [{
            data: countBookCommunity,
            backgroundColor: [
                '#4e73df', '#E8743B', '#19A979', '#ED4A7B',
                '#945ECF', '#13A4B4', '#525DF4', '#BF399E',
                '#6C8893', '#EE6868'],
            hoverBorderColor: "rgba(234, 236, 244, 1)",
        }],
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        tooltips: {
            backgroundColor: "rgb(255,255,255)",
            bodyFontColor: "#2e2e2e",
            borderColor: '#dddfeb',
            borderWidth: 1,
            xPadding: 15,
            yPadding: 15,
            displayColors: false,
            caretPadding: 10,
        },
        legend: {
            display: false,
            position: "bottom"
        },
        cutoutPercentage: 30
    },
});