package ru.hldns.klaw.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import ru.hldns.klaw.parsers.JobListener;
@Slf4j
@Configuration
public class BatchConfiguration {

    @Bean
    public JobLauncher asyncJobLauncher(JobRepository jobRepository,TaskExecutor taskExecutor) {
        SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
        jobLauncher.setJobRepository(jobRepository);
        jobLauncher.setTaskExecutor(taskExecutor);
        return jobLauncher;
    }

    @Bean("autoUpdateJob")
    public Job autoUpdateJob(JobBuilderFactory jobs,
                             Step parseInpxStep,
                             Step parseFB2FilesStep) {
        return jobs.get("autoUpdateJob")
                .incrementer(new RunIdIncrementer())
                .start(parseInpxStep)
                .next(parseFB2FilesStep)
                .listener(new JobExecutionListener() {
                    @Override
                    public void beforeJob(JobExecution jobExecution) {
                        log.info("Start autoUpdateJob");
                    }

                    @Override
                    public void afterJob(JobExecution jobExecution) {
                        log.info("End autoUpdateJob");
                    }
                })
                .build();
    }

}
