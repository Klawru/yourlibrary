package ru.hldns.klaw.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.PlatformTransactionManager;
import ru.hldns.klaw.data.model.Book;
import ru.hldns.klaw.data.repository.BookRepository;
import ru.hldns.klaw.parsers.ChunkCountListener;
import ru.hldns.klaw.parsers.JobListener;
import ru.hldns.klaw.parsers.fb2.BookFileProcessor;
import ru.hldns.klaw.parsers.fb2.FB2PairWriter;
import ru.hldns.klaw.parsers.inpx.model.ParserServiceDAO;
import ru.hldns.klaw.service.CacheManagerService;
import ru.hldns.klaw.service.FileService;
import ru.hldns.klaw.web.service.CoverPageService;

import java.util.Collections;

@Slf4j
@Configuration
public class FB2ParserJobConfig {

    private static final int CHUNK_SIZE = 100;

    @Bean("BookRepositoryReader")
    @StepScope
    public RepositoryItemReader<Book> getBookRepositoryReader(BookRepository repository) {
        RepositoryItemReader<Book> reader = new RepositoryItemReader<>();
        reader.setRepository(repository);
        reader.setMethodName("findAllByDeletedFalse");
        reader.setPageSize(CHUNK_SIZE * 10);
        reader.setSaveState(false);
        reader.setSort(Collections.singletonMap("folder", Sort.Direction.ASC));
        return reader;
    }

    @Bean("BookFileProcessor")
    @StepScope
    public BookFileProcessor getBookItemProcessor(FileService fileService, CoverPageService coverPageService, MessageSource messageSource) {
        return new BookFileProcessor(fileService, coverPageService, messageSource);
    }

    @Bean("EntityWriter")
    @StepScope
    public FB2PairWriter getEntityWriter(ParserServiceDAO service) {
        return new FB2PairWriter(service);
    }

    @Bean("parseFB2FilesJob")
    public Job parseFB2FilesJob(JobBuilderFactory jobs, Step parseFB2FilesStep, CacheManagerService cacheManagerService, JobListener jobListener) {
        return jobs.get("parseFB2FilesJob")
                .incrementer(new RunIdIncrementer())
                .flow(parseFB2FilesStep)
                .end()
                .listener(jobListener)
                .build();
    }

    @Bean("parseFB2FilesStep")
    public Step parseFB2FilesStep(StepBuilderFactory stepBuilderFactory,
                                  @Qualifier("BookRepositoryReader") ItemReader<Book> reader,
                                  @Qualifier("EntityWriter") ItemWriter<Book> entityWriter,
                                  @Qualifier("BookFileProcessor") ItemProcessor<Book, Book> processor,
                                  TaskExecutor taskExecutor,
                                  PlatformTransactionManager transactionManager) {
        return stepBuilderFactory.get("parseFB2FilesStep")
                .<Book, Book>chunk(CHUNK_SIZE)
                .reader(reader)
                .processor(processor)
                .writer(entityWriter)
                .taskExecutor(taskExecutor)
                .throttleLimit(4)
                .transactionManager(transactionManager)
                .allowStartIfComplete(true)
                .listener(new ChunkCountListener("FB2 items", CHUNK_SIZE * 10))
                .build();
    }

}
