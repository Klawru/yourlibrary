package ru.hldns.klaw.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/admin/**", "/actuator**").hasRole("ADMIN")
                .antMatchers("/add/**", "/edit/**").hasRole("ADMIN")
                .antMatchers("/book/**/download").hasAuthority("CAN_DOWNLOAD")
                .antMatchers("/settings/**").authenticated()
                .antMatchers("**").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin().loginPage("/login").permitAll()
                .and()
                .logout().logoutUrl("/logout")
                .permitAll();
    }

}
