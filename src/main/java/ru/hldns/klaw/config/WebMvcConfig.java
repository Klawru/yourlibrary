package ru.hldns.klaw.config;

import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.CacheControl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    private final String imgUploadDir;
    private final String thumbnailDir;

    public WebMvcConfig(ApplicationConfig applicationConfig) {
        imgUploadDir = getAbsolutePath(applicationConfig.getImgUploadDir());
        thumbnailDir = getAbsolutePath(applicationConfig.getThumbnailDir());
    }

    private String getAbsolutePath(String path) {
        return Paths.get(path).toAbsolutePath().normalize()
                .toString().replace("\\", "/");
    }

    @Bean
    public WebServerFactoryCustomizer<TomcatServletWebServerFactory> containerCustomizer() {
        return new EmbeddedTomcatCustomizer();
    }

    private static class EmbeddedTomcatCustomizer implements WebServerFactoryCustomizer<TomcatServletWebServerFactory> {

        @Override
        public void customize(TomcatServletWebServerFactory factory) {
            factory.addConnectorCustomizers(connector -> {
                connector.setProperty("relaxedPathChars", "<>[\\]^`{|}");
                connector.setProperty("relaxedQueryChars", "<>[\\]^`{|}");
            });
        }
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(new Locale.Builder().setLanguage("ru").setScript("Cyrl").build());
        return slr;
    }

    @Bean
    public Function<String, String> currentUrlWithoutParam() {
        return param -> URLDecoder.decode(
                ServletUriComponentsBuilder.fromCurrentRequest()
                        .replaceQueryParam(param).build()
                        .encode(Charset.defaultCharset()).toString()
                , StandardCharsets.UTF_8);
    }


    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/covers/**")
                .addResourceLocations("file:///" + imgUploadDir + "/")
                .setCacheControl(
                        CacheControl.empty()
                                .cachePublic().sMaxAge(30, TimeUnit.DAYS))
                .setCachePeriod(2628000);
        registry.addResourceHandler("/thumbnail/**")
                .addResourceLocations("file:///" + thumbnailDir + "/")
                .setCacheControl(
                        CacheControl.empty()
                                .cachePublic().sMaxAge(30, TimeUnit.DAYS))
                .setCachePeriod(2628000);
    }
}
