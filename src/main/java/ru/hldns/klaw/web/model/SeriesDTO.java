package ru.hldns.klaw.web.model;

public interface SeriesDTO{
    Integer getId();
    String getName();
}