package ru.hldns.klaw.web.model;

public interface CountingResult {
    String getResult();

    long getCount();
}
