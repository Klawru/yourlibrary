package ru.hldns.klaw.web.model;

public interface GenreDTOMin {
    Integer getId();

    String getName();
}
