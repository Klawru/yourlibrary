package ru.hldns.klaw.web.model;

public interface AuthorNameDTO {
    String getFullName();

    String getDisplayName();
}