package ru.hldns.klaw.web.model;

import java.util.Set;

public interface AuthorBooksDTO {
    Integer getId();

    String getFullName();

    Set<AuthorBookDTO> getBooks();

    interface AuthorBookDTO {
        Integer getId();

        String getTitle();

        SeriesDTO getSeries();

        int getSeriesNumber();

        String getLang();

        String getDisplayFileSize();
    }
}