package ru.hldns.klaw.web.model;

public interface BookDTO {
    Integer getId();

    String getTitle();

    String getSeries();

    String getSeriesNumber();

    String getCoverPage();

    String getDate();

    String getAuthors();

    String getGenres();

    String getLang();

    String getDisplayFileSize();

    default boolean hasCoverPage(){
        return getCoverPage()!=null;
    }

    default boolean hasSeries(){
        return getSeries()!=null;
    }
}