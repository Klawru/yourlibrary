package ru.hldns.klaw.web;

import lombok.Builder;
import lombok.Data;
import org.hibernate.search.query.facet.Facet;
import ru.hldns.klaw.utility.Pagination;

import java.util.Collections;
import java.util.List;

@Data
public class SearchingResults {
    private boolean hasPartialResults;
    private List resultList = Collections.emptyList();
    private List<Facet> authorsFacet;
    private List<Facet> communityFacet;
    private int resultSize = 0;
    private int currentPage = 1;
    private Pagination pagination;

    public Pagination getPagination() {
        if (pagination == null)
            pagination = Pagination.of(currentPage, resultSize, 3);
        return pagination;
    }
}
