package ru.hldns.klaw.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.hldns.klaw.service.search.HibernateSearchService;
import ru.hldns.klaw.web.controller.SearchController;
import ru.hldns.klaw.web.SearchingResults;

@Service
public class SearchPageService {

    private final HibernateSearchService searchService;

    @Autowired
    public SearchPageService(HibernateSearchService searchService) {
        this.searchService = searchService;
    }

    public SearchingResults search(SearchController.SearchForm form, int pageNum, int resultOnPage){
        return searchService.mainSearch(
                form.query, form.community,
                form.filters.toArray(SearchController.SearchForm.Filter[]::new), pageNum, resultOnPage);
    }

}
