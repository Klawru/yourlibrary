package ru.hldns.klaw.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.hldns.klaw.data.model.Community;
import ru.hldns.klaw.data.model.Genre;
import ru.hldns.klaw.data.repository.BookRepository;
import ru.hldns.klaw.data.repository.CommunityRepository;
import ru.hldns.klaw.data.repository.GenreRepository;
import ru.hldns.klaw.utility.Pagination;
import ru.hldns.klaw.web.model.BookDTO;
import ru.hldns.klaw.web.model.GenreDTOMin;
import ru.hldns.klaw.web.results.ResultList;

import java.util.List;
import java.util.Optional;

import static ru.hldns.klaw.web.WebUtility.getPageNum;

@Service
public class GenreService {

    final private GenreRepository genreRepository;
    final private BookRepository bookRepository;
    final private CommunityRepository communityRepository;

    @Autowired
    public GenreService(GenreRepository genreRepository, BookRepository bookRepository, CommunityRepository communityRepository) {
        this.genreRepository = genreRepository;
        this.bookRepository = bookRepository;
        this.communityRepository = communityRepository;
    }

    public Optional<Genre> findGenreById(int id) {
        return genreRepository.findById(id);
    }

    @Transactional
    public Community addGenre(Genre genre) {
        Community parent = communityRepository.getOne(genre.getCommunity().getId());
        parent.addGenre(genre);
        return communityRepository.save(parent);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public ResultList<GenreDTOMin, List<BookDTO>> findBooksByGenre(int genreId, int page, int pageSize) {
        Optional<GenreDTOMin> byId = genreRepository.getById(genreId);
        if (byId.isEmpty())
            return ResultList.empty();
        final var genre = byId.get();
        return getBookDTOByGenre(page, pageSize, genre);
    }

    private ResultList<GenreDTOMin, List<BookDTO>> getBookDTOByGenre(int page, int pageSize, GenreDTOMin genre) {
        List<BookDTO> list = bookRepository.findByGenre(genre.getId(), pageSize, getPageNum(page) * pageSize);
        int resultSize = bookRepository.countBookByGenresIs(genre.getId());
        return ResultList.of(genre, list, Pagination.of(page, resultSize, pageSize, 3));
    }

    public Genre saveGenre(Genre genre) {
        return genreRepository.save(genre);
    }
}
