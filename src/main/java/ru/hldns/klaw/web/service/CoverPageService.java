package ru.hldns.klaw.web.service;

import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.tasks.UnsupportedFormatException;
import org.springframework.stereotype.Service;
import ru.hldns.klaw.config.ApplicationConfig;
import ru.hldns.klaw.utility.CommonUtility;
import ru.hldns.klaw.utility.Either;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
@Service
public class CoverPageService {

    private final String imgPath;
    private final String thumbnailPath;

    public CoverPageService(ApplicationConfig applicationConfig) {
        this.imgPath = Path.of(applicationConfig.getImgUploadDir()).toAbsolutePath().normalize().toString();
        this.thumbnailPath = Path.of(applicationConfig.getThumbnailDir()).toAbsolutePath().normalize().toString();
    }

    public Either<String, Exception> save(byte[] binary, String md5) {
        try {
            final var newFilePath = CommonUtility.getPathFromMD5(md5, "jpg");
            final BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(binary));
            saveImage(bufferedImage, newFilePath);
            saveThumbnail(bufferedImage, newFilePath);
            return Either.success(newFilePath);
        } catch (Exception e) {
            log.error("On save image file:", e);
            return Either.error(e);
        }
    }

    private void saveImage(BufferedImage bufferedImage, String newFilePath) throws IOException {
        saveImage(bufferedImage, Paths.get(imgPath, newFilePath), 1200, 1200);
    }

    private void saveThumbnail(BufferedImage bufferedImage, String filePath) throws IOException {
        saveImage(bufferedImage, Paths.get(thumbnailPath, filePath), 150, 150);
    }

    private void saveImage(BufferedImage image, Path path , int height, int width) throws IOException {
        Files.createDirectories(path.getParent());
        final File imgFile = path.toFile();
        try {
            if (!imgFile.exists())
                if ((image.getHeight() > height) || (image.getWidth() > width))
                    Thumbnails.of(image).size(width, height).outputFormat("jpg").outputQuality(0.92f).toFile(imgFile);
                else
                    Thumbnails.of(image).size(image.getWidth(), image.getHeight()).outputFormat("jpg").outputQuality(0.92f).toFile(imgFile);
        } catch (IIOException | UnsupportedFormatException e) {
            log.error("Exception: '{}'. File: '{}'.", e.getLocalizedMessage(), path);
        }
    }

}
