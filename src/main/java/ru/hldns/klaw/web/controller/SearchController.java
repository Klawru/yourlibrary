package ru.hldns.klaw.web.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.hldns.klaw.data.model.Community;
import ru.hldns.klaw.web.SearchingResults;
import ru.hldns.klaw.web.service.CommunityService;
import ru.hldns.klaw.web.service.SearchPageService;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/search")
@Slf4j
public class SearchController {
    private static final int RESULT_ON_PAGE = 15;
    private final CommunityService communityService;
    private final SearchPageService service;

    @Autowired
    public SearchController(CommunityService communityService, SearchPageService service) {
        this.communityService = communityService;
        this.service = service;
    }

    @ModelAttribute("communities")
    public List<Community> populateMetadataFieldsByText() {
        return communityService.findAllCommunity();
    }

    @RequestMapping(method = RequestMethod.GET)
    public String search(SearchForm form, Model model, BindingResult bindingResult,
                         @RequestParam(value = "page", required = false, defaultValue = "1") Integer pageNum) {
        if (bindingResult.hasErrors()) return "search";
        SearchingResults result = service.search(form, pageNum, RESULT_ON_PAGE);
        model.addAttribute("result", result);
        model.addAttribute("pagination", result.getPagination());
        return "search";
    }

    @Data
    @AllArgsConstructor
    public static class SearchForm implements Cloneable, Serializable {
        public final static Filter.FieldType[] fieldType = Filter.FieldType.values();
        public final static Filter.FilterType[] filterType = Filter.FilterType.values();
        public final List<Filter> filters = new ArrayList<>(5);
        @NotNull
        @Size(min = 3, max = 100)
        public String query;
        public int community;

        public SearchForm() {
            for (int i = 0; i < 5; i++) {
                filters.add(new Filter());
            }
        }

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        public static class Filter implements Cloneable, Serializable {
            public FieldType field = FieldType.author;
            public FilterType filter = FilterType.should;
            public String value;


            public enum FieldType {
                author, series, issued, title, lang
            }

            public enum FilterType {
                nonEquals, should, equals
            }
        }
    }
}
