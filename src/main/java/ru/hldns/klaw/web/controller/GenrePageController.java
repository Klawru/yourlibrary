package ru.hldns.klaw.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.hldns.klaw.web.service.GenreService;

@Controller
public class GenrePageController {
    private final GenreService genreService;

    @Autowired
    public GenrePageController(GenreService genreService) {
        this.genreService = genreService;
    }

    @GetMapping("/genre/{id}/")
    public String getGenre(@PathVariable("id") int id, @RequestParam(required = false, defaultValue = "1") Integer page, Model model) {
        final var booksByGenre = genreService.findBooksByGenre(id, page, 20);
        if (booksByGenre.isEmpty())
            return "redirect:/error/404";
        model.addAttribute("genre", booksByGenre.getObject());
        model.addAttribute("books", booksByGenre.getList());
        model.addAttribute("pagination", booksByGenre.getPagination());
        return "genre";
    }
}
