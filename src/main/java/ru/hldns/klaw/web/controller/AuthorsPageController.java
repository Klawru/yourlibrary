package ru.hldns.klaw.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.hldns.klaw.web.model.AuthorDTO;
import ru.hldns.klaw.web.model.BookDTOMin;
import ru.hldns.klaw.web.model.SeriesDTO;
import ru.hldns.klaw.web.results.Result;
import ru.hldns.klaw.web.results.ResultList;
import ru.hldns.klaw.web.service.AuthorsService;

import java.util.List;

@Controller
public class AuthorsPageController {

    private final AuthorsService authorService;

    @Autowired
    public AuthorsPageController(AuthorsService authorService) {
        this.authorService = authorService;
    }

    @RequestMapping(value = "/author", method = RequestMethod.GET)
    public String getBooksPage(@RequestParam(value = "name") String fullName,
                               @RequestParam(defaultValue = "1") Integer page,
                               Model model) {
        final ResultList<AuthorDTO, Page<BookDTOMin>> author = authorService.getBooksByAuthor(fullName, page, 20);
        if (author.isEmpty())
            return "redirect:error/404";
        model.addAttribute("author", author.getObject());
        model.addAttribute("books", author.getList().getContent());
        model.addAttribute("pagination", author.getPagination());
        return "authorBooks";
    }

    @RequestMapping(value = "/author/series", method = RequestMethod.GET)
    public String getSeriesPage(@RequestParam("name") String fullName, Model model) {
        final Result<AuthorDTO, List<SeriesDTO>> author = authorService.getSeriesByAuthor(fullName);
        if (author.isEmpty())
            return "redirect:error/404";
        model.addAttribute("author", author.getMainObject());
        model.addAttribute("series", author.getResults());
        return "authorSeries";
    }
}
