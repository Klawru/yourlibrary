package ru.hldns.klaw.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.hldns.klaw.data.model.Series;
import ru.hldns.klaw.web.model.BookDTOMin;
import ru.hldns.klaw.web.results.PaginatedResult;
import ru.hldns.klaw.web.results.Result;
import ru.hldns.klaw.web.service.BookSeriesService;

@Controller
public class BookSeriesController {
    private final BookSeriesService service;

    @Autowired
    public BookSeriesController(BookSeriesService controler) {
        this.service = controler;
    }

    @RequestMapping(value = "/bookSeries/", method = RequestMethod.GET)
    public String getBookSeriesPage(@RequestParam("name") String name, Model model) {
        final Result<BookDTOMin.SeriesDTO, BookDTOMin[]> seriesOptional = service.findBookBySeries(name);
        if (seriesOptional.isEmpty())
            return "redirect:/error/404";
        model.addAttribute("series", seriesOptional.getMainObject());
        model.addAttribute("books", seriesOptional.getResults());
        return "bookSeries";
    }

    @RequestMapping(value = "/bookSeries", method = RequestMethod.GET)
    public String getBookSeriesByLetter(@RequestParam(value = "name", defaultValue = "a") String letter,
                                        @RequestParam(defaultValue = "1") Integer page, Model model) {
        final PaginatedResult<Page<Series>> series = service.findBookSeriesByLetter(letter, page, 20);
        model.addAttribute("seriesList", series.getResult());
        model.addAttribute("pagination", series.getPagination());
        model.addAttribute("letter", letter.charAt(0));
        return "BookSeriesList";
    }
}
