package ru.hldns.klaw.web.controller;

import lombok.NonNull;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;
import ru.hldns.klaw.utility.Pair;
import ru.hldns.klaw.web.service.BookPageService;

import javax.mail.internet.MimeUtility;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@Controller
public class BookController {
    private final BookPageService bookPageService;

    public BookController(BookPageService bookPageService) {
        this.bookPageService = bookPageService;
    }


    @RequestMapping(value = "/book/{id}", method = RequestMethod.GET)
    public String getItem(@PathVariable("id") int id, Model model) {
        return bookPageService.getBook(id).map(book -> {
            model.addAttribute("book", book);
            return "book";
        }).orElse("redirect:/error/404");
    }

    @RequestMapping(value = "/book/{idBook}/{idFile}/download", method = RequestMethod.GET)
    @ResponseBody
    public StreamingResponseBody getFile(@PathVariable int idBook,
                                         HttpServletResponse resp,
                                         @PathVariable String idFile) throws IOException {
        final Optional<Pair<StreamingResponseBody, String>> book = bookPageService.getStreamingResponseBook(idBook, idFile);
        if (book.isPresent()) {
            final @NonNull String filename = book.get().getSecond();
            final @NonNull StreamingResponseBody responseBody = book.get().getFirst();
            resp.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"" + MimeUtility.encodeWord(filename, "utf-8", "Q") + "\"");
            return responseBody;
        }
        return sendNotFoundError(resp);
    }

    @SuppressWarnings("SameReturnValue")
    private StreamingResponseBody sendNotFoundError(HttpServletResponse resp) throws IOException {
        resp.sendError(HttpServletResponse.SC_NOT_FOUND, "Cannot find file");
        return null;
    }

}
