package ru.hldns.klaw.utility;

public class CommonUtility {
    public static int getProcessorCore() {
        return Runtime.getRuntime().availableProcessors();
    }

    public static String getPathFromMD5(String MD5,String extension){
        return "/" + MD5.substring(0, 2) + "/" + MD5.substring(2, 4) + "/" + MD5.substring(4) + "." + extension;
    }

    /**
     * This method is simple since FB2 only supports "image/jpeg" and "image/png".
     * @param mime is a two-part identifier for file formats and format contents transmitted on the Internet.
     * @return filename extension
     */
    public static String getExtension(String mime){
        return mime.substring(mime.indexOf("/")+1);
    }
}
