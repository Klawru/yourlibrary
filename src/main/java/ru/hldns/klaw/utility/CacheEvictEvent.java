package ru.hldns.klaw.utility;

import org.springframework.context.ApplicationEvent;

public class CacheEvictEvent extends ApplicationEvent {
    public CacheEvictEvent(Object source) {
        super(source);
    }
}
