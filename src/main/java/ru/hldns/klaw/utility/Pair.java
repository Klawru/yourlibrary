package ru.hldns.klaw.utility;

import lombok.*;

import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Value
public final class Pair<S, T> {

    private final @NonNull S first;
    private final @NonNull T second;

    public static <S, T> Collector<org.springframework.data.util.Pair<S, T>, ?, Map<S, T>> toMap() {
        return Collectors.toMap(org.springframework.data.util.Pair::getFirst, org.springframework.data.util.Pair::getSecond);
    }
}

