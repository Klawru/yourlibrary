package ru.hldns.klaw.utility;

import org.apache.commons.io.FileUtils;

import java.util.Optional;

public class FileUtility {

    public static String bytesToString(long bytes) {
        return FileUtils.byteCountToDisplaySize(bytes);
    }

    public static String getExtension(String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf(".") + 1)).orElse("");
    }

    public static String getWithoutExtension(String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(0, filename.lastIndexOf("."))).orElse(filename);
    }
}
