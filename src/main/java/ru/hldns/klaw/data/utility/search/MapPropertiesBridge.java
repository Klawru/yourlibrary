package ru.hldns.klaw.data.utility.search;

import org.apache.lucene.document.Document;
import org.hibernate.search.bridge.FieldBridge;
import org.hibernate.search.bridge.LuceneOptions;

import java.util.Map;

public class MapPropertiesBridge implements FieldBridge {

    @Override
    public void set(String fieldName, Object value, Document document, LuceneOptions luceneOptions) {
        if (value != null) {
            indexNotNullMap(fieldName, value, document, luceneOptions);
        }
    }

    @SuppressWarnings("unchecked")
    private void indexNotNullMap(String fieldName, Object value, Document document, LuceneOptions luceneOptions) {
        for (Map.Entry<String, String> entry : ((Map<String, String>) value).entrySet()) {
            luceneOptions.addFieldToDocument(entry.getKey(), entry.getValue(), document);
        }
    }
}
