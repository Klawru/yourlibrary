package ru.hldns.klaw.data.utility.search;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexableField;
import org.hibernate.search.bridge.LuceneOptions;
import org.hibernate.search.bridge.TwoWayFieldBridge;
import ru.hldns.klaw.data.model.Author;

import java.util.Set;
import java.util.StringJoiner;

public class AuthorBridge implements TwoWayFieldBridge {
    @Override
    public Object get(String name, Document document) {
        final IndexableField field = document.getField(name);
        if (field != null) {
            return field.stringValue();
        } else {
            return "";
        }
    }

    @Override
    public String objectToString(Object object) {
        @SuppressWarnings("unchecked")
        Set<Author> authorSet = (Set<Author>) object;
        StringJoiner joiner = new StringJoiner(", ");
        for (Author author : authorSet) {
            joiner.add(author.getDisplayName());
        }
        return joiner.toString();
    }

    @Override
    public void set(String name, Object value, Document document, LuceneOptions luceneOptions) {
        String indexedString = objectToString(value);
        if (indexedString == null && luceneOptions.indexNullAs() != null) {
            indexedString = luceneOptions.indexNullAs();
        }
        luceneOptions.addFieldToDocument(name, indexedString, document);
    }
}
