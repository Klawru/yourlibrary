package ru.hldns.klaw.data.utility;

import org.hibernate.boot.MetadataBuilder;
import org.hibernate.boot.spi.MetadataBuilderContributor;
import org.hibernate.dialect.function.VarArgsSQLFunction;
import org.hibernate.type.TextType;

public class SqlFunctionBuilderContributor implements MetadataBuilderContributor {
    @Override
    public void contribute(MetadataBuilder metadataBuilder) {
        metadataBuilder.applySqlFunction(
                "concat_ws",
                new VarArgsSQLFunction(new TextType(), "concat_ws(", ",", ")"));
    }
}
