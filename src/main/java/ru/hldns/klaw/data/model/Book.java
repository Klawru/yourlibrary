package ru.hldns.klaw.data.model;

import com.vladmihalcea.hibernate.type.basic.YearType;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.*;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.*;
import ru.hldns.klaw.data.utility.MetaDataField;
import ru.hldns.klaw.data.utility.search.AuthorBridge;
import ru.hldns.klaw.data.utility.search.MapPropertiesBridge;
import ru.hldns.klaw.service.search.BookIndexingInterceptor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.Year;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;


@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "libId")
@ToString(of = {"id", "title", "lang", "file", "libId"})
@Indexed(interceptor = BookIndexingInterceptor.class)
@Entity
@Table(name = "books")
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@TypeDef(typeClass = YearType.class, defaultForType = Year.class)
public class Book implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "book_generator")
    @SequenceGenerator(name = "book_generator",
            sequenceName = "book_seq", allocationSize = 10)
    @Column(name = "id_book", updatable = false, nullable = false)
    @With
    private Integer id;

    @Field(store = Store.YES)
    @Column(name = "name", nullable = false, length = 250)
    private String title;

    @IndexedEmbedded
    @Field(bridge = @FieldBridge(impl = AuthorBridge.class), store = Store.YES, analyze = Analyze.NO, index = Index.NO)
    @ManyToMany(cascade = {CascadeType.MERGE})
    @JoinTable(name = "books2authors",
            joinColumns = @JoinColumn(name = "id_book"),
            inverseJoinColumns = @JoinColumn(name = "id_author"),
            indexes = @javax.persistence.Index(name = "author_index", columnList = "id_author"))
    private Set<Author> authors;

    @IndexedEmbedded
    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "genres2books",
            joinColumns = @JoinColumn(name = "id_book"),
            inverseJoinColumns = @JoinColumn(name = "id_genre"),
            indexes = @javax.persistence.Index(name = "genres_index", columnList = "id_genre"))
    private Set<Genre> genres;

    @IndexedEmbedded
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH})
    private Series series;

    @Column(name = "series_number")
    private int seriesNumber;

    /**
     * ISO 639 https://en.wikipedia.org/wiki/ISO_639
     */
    @Field(analyze = Analyze.NO, store = Store.YES)
    @Column(name = "lang", length = 4)
    private String lang;

    @Column(name = "deleted")
    private boolean deleted;

    /**
     * Filename without extension.
     */
    private String file;

    /**
     * File extension without front dot.
     */
    @Column(name = "file_extension", length = 4)
    private String fileExtension;

    /**
     * Name of the inp file and archive where the book is stored, without the file extension
     */
    @Column(name = "folder", length = 500)
    private String folder;

    /**
     * Date of book publication or adding to library.
     */
    @Field(analyze = Analyze.NO, store = Store.YES, index = Index.NO)
    private LocalDate date;

    /**
     * File size in bytes.
     */
    @Column(name = "file_size")
    private int fileSize;

    /**
     * File size as string to display .
     */
    @Column(name = "file_size_string", length = 10)
    @Field(analyze = Analyze.NO, store = Store.YES, index = Index.NO)
    private String displayFileSize;

    private int libId;

    @Field(bridge = @FieldBridge(impl = MapPropertiesBridge.class))
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @Basic(fetch = FetchType.LAZY)
    private Map<String, String> properties;


    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @Basic(fetch = FetchType.LAZY)
    private String description;

    /**
     * Annotation for this book"
     */
    @Column(columnDefinition = "text")
    protected String annotation;

    @Column(columnDefinition = "smallint")
    private Year publishedDate;

    @Column(length = 40)
    @Field(analyze = Analyze.NO, store = Store.YES, index = Index.NO)
    private String coverPage;

    private String publisher;

    public void addMetadata(MetaDataField key, String value) {
        if (properties == null)
            properties = new HashMap<>();
        properties.put(key.getName(), value);
    }

    public void removeMetadata(MetaDataField key) {
        if (properties != null)
            properties.remove(key.getName());
    }

    public void addAuthors(Author author) {
        if (authors == null)
            authors = new LinkedHashSet<>(3);
        authors.add(author);
    }

    public void removeAuthors(Author author) {
        if (authors == null)
            authors = new LinkedHashSet<>(3);
        authors.remove(author);
    }

    public String getFileName() {
        return this.getFile() + "." + this.getFileExtension();
    }

    public String getDisplayFileName() {
        return this.getTitle() + "." + this.getFileExtension();
    }


    public boolean hasCoverPage() {
        return coverPage != null;
    }

    public boolean hasSeries() {
        return series != null;
    }

    public boolean hasAnnotation() {
        return annotation != null;
    }

}