package ru.hldns.klaw.data.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@NoArgsConstructor
@Table(name = "series_author")
@Entity
@Immutable
public class SeriesAuthor {
    @Id
    @Column(name = "id_series", updatable = false, nullable = false)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "id_author")
    private Integer idAuthor;
}
