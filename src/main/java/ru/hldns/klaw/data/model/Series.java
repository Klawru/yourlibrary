package ru.hldns.klaw.data.model;

import lombok.*;
import org.hibernate.search.annotations.ContainedIn;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Store;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Data
@ToString(of = {"id", "name"})
@EqualsAndHashCode(of = "name")
@Entity
@Table(name = "series")
@NoArgsConstructor
@AllArgsConstructor
@NamedEntityGraph(name = "series-books",
        attributeNodes = @NamedAttributeNode("books"))
public class Series implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "series_seq")
    @SequenceGenerator(name = "series_seq", sequenceName = "series_seq", allocationSize = 10)
    @Column(name = "id_series", updatable = false, nullable = false)
    @With
    Integer id;

    @Field(store = Store.YES)
    @Column(unique = true, length = 250)
    public String name;

    @ContainedIn
    @OneToMany(mappedBy = "series", orphanRemoval = true)
    private Set<Book> books;

    public Series(String name) {
        this.name = name;
    }
}
