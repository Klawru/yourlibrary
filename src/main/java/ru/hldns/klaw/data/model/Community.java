package ru.hldns.klaw.data.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.bridge.builtin.IntegerBridge;
import ru.hldns.klaw.data.model.Genre;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "community")
@Data
@EqualsAndHashCode(of = {"id", "name"})
@ToString(of = {"id", "name"})
public class Community implements Serializable {

    @Field(name = "communityId", store = Store.YES)
    @NumericField(forField = "communityId")
    @Field(name = "communityId_facet", analyze = Analyze.NO,bridge =@FieldBridge(impl = IntegerBridge.class))
    @Facet(name = "communityFacetId",forField = "communityId_facet",encoding = FacetEncodingType.STRING)
    @Id
    @Column(name = "id_community",updatable = false,nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    @Field(analyze = Analyze.NO,store = Store.YES,index = Index.NO)
    @Column(name = "name")
    @Size(min = 1, max = 150)
    private String name;

    @ContainedIn
    @OneToMany(mappedBy = "community", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Genre> genres;


    public void addGenre(Genre genre) {
        if (genres==null)
            genres=new HashSet<>();
        genres.add(genre);
        genre.setCommunity(this);
    }

    public void removeGenre(Genre genre) {
        if (genres!=null)
            genres.remove(genre);
        genre.setCommunity(null);
    }

}
