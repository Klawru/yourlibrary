package ru.hldns.klaw.data.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;


@Table(name = "last_update_table")
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"name"})
@EqualsAndHashCode(of = "name")
public class UpdateInfo {
    @Id
    String name;
    Long bookCount;
    Instant updated;
}
