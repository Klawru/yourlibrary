package ru.hldns.klaw.data.model;

import lombok.*;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.ContainedIn;
import org.hibernate.search.annotations.Facet;
import org.hibernate.search.annotations.Field;
import org.springframework.data.annotation.PersistenceConstructor;

import javax.persistence.*;
import java.util.Set;
import java.util.StringJoiner;

@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"firstName", "middleName", "lastName"})
@ToString(of = {"id", "fullName"})
@Entity
@Table(name = "authors")
@NamedEntityGraph(name = "author-books",
        attributeNodes = {
                @NamedAttributeNode(value = "books", subgraph = "book-series")
        },
        subclassSubgraphs = {
                @NamedSubgraph(
                        name = "book-series",
                        attributeNodes = @NamedAttributeNode(value = "series"))
        })
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "author_generator")
    @SequenceGenerator(name = "author_generator", sequenceName = "author_seq", allocationSize = 10)
    @Column(name = "id_author", updatable = false, nullable = false)
    @With
    private Integer id;

    @Column(name = "last_name", nullable = false, length = 128)
    private String lastName;

    @PersistenceConstructor
    public Author(Integer id, String lastName, String firstName, String middleName, String fullName, Set<Book> books) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.fullName = fullName;
        this.books = books;
    }

    @Column(name = "first_name", length = 128)
    private String firstName;

    @Column(name = "middle_name", length = 128)
    private String middleName;

    @Field(name = "fullName")
    @Field(name = "authorFacet", analyze = Analyze.NO)
    @Facet(name = "authorFacet", forField = "authorFacet")
    @Column(name = "full_name", length = 400, unique = true)
    String fullName;

    @PreUpdate
    @PrePersist
    void onPersist() {
        createFullName();
    }

    @ContainedIn
    @ManyToMany(mappedBy = "authors")
    Set<Book> books;

    public static Author of(String lastName) {
        final var author = new Author(null, lastName, null, null, null, null);
        author.createFullName();
        return author;
    }

    public static Author of(String lastName, String firstName) {
        final var author = new Author(null, lastName, firstName, null, null, null);
        author.createFullName();
        return author;
    }

    public static Author of(String lastName, String firstName, String middleName) {
        final var author = new Author(null, lastName, firstName, middleName, null, null);
        author.createFullName();
        return author;
    }

    private void createFullName() {
        StringJoiner joiner = new StringJoiner(" ");
        if (!(firstName == null) && !firstName.isEmpty()) {
            if (firstName.length() == 1)
                firstName = firstName + '.';
            joiner.add(firstName);
        }
        if (!(middleName == null) && !middleName.isEmpty()) {
            if (middleName.length() == 1)
                middleName = middleName + '.';
            joiner.add(middleName);
        }
        if (!lastName.isEmpty()) {
            joiner.add(lastName);
        }
        fullName = joiner.toString();
    }

    public String getDisplayName() {
        if (firstName != null)
            return firstName + ' ' + lastName;
        else
            return lastName;
    }
}
