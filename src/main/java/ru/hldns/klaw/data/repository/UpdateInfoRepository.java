package ru.hldns.klaw.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.hldns.klaw.data.model.UpdateInfo;

public interface UpdateInfoRepository extends JpaRepository<UpdateInfo, String> {

}
