package ru.hldns.klaw.data.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.hldns.klaw.data.model.Author;
import ru.hldns.klaw.web.model.AuthorNameDTO;
import ru.hldns.klaw.web.model.CountingResult;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface AuthorsRepository extends JpaRepository<Author, Long> {

    Optional<Author> findByFullNameIs(String fullName);

    List<Author> findByFullNameIn(Collection<String> fullNames);

    <T> Optional<T> findByFullName(String fullName, Class<T> type);

    @Query("SELECT a FROM Author a JOIN a.books b WHERE a.fullName like ?1 AND b.lang=?2")
    <T> Optional<T> findByFullNameAndBookLang(String fullName, String lang, Class<T> type);

    @Query("SELECT FUNCTION('concat_ws', ' ', a.lastName, a.firstName, a.middleName) as fullName FROM Author a WHERE FUNCTION('f_unaccent', a.lastName) like ?1% ORDER BY FUNCTION('concat_ws', ' ', a.lastName, a.firstName, a.middleName)")
    Page<AuthorNameDTO> findByFullNameLike(String letter, Pageable pageable);

    @Query(value = "SELECT concat_ws(' ', a.last_name, a.first_name, a.middle_name) as fullName FROM authors a WHERE f_unaccent(a.last_name) ~>~ 'я%' and f_unaccent(a.last_name) !~~ 'я%' or f_unaccent(a.last_name) ~<~ 'a%' order by fullName", nativeQuery = true)
    Page<AuthorNameDTO> findByFullNameOther(Pageable pageable);

    /**
     * @return top authors by number of books
     */
    @SuppressWarnings("SpringDataRepositoryMethodReturnTypeInspection")
    @Query(value = "SELECT a.full_name as result, COUNT(a.full_name) as count " +
            "FROM authors a " +
            "LEFT JOIN books2authors b2a on a.id_author = b2a.id_author " +
            "GROUP BY a.full_name " +
            "ORDER BY COUNT(a.full_name) DESC " +
            "LIMIT ?1",
            nativeQuery = true)
    List<CountingResult> getTopByCountBooks(int top);
}
