package ru.hldns.klaw.parsers.fb2.model;

import lombok.Data;

@Data
public class ImageType {
    protected String type;
    protected String href;
    protected String alt;
    protected Binary binary;
}
