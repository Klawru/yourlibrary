package ru.hldns.klaw.parsers.fb2.file;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

abstract class FB2AbstractParser {

    static final String ELEMENT_ID = "id";
    //DESCRIPTION main tags
    static final String DESCRIPTION = "description";
    static final String TITLE_INFO = "title-info";
    static final String SRC_TITLE_INFO = "src-title-info";
    static final String DOCUMENT_INFO = "document-info";
    static final String PUBLISH_INFO = "publish-info";
    static final String CUSTOM_INFO = "custom-info";
    //TitleInfo tags
    static final String GENRE = "genre";
    static final String AUTHOR = "author";
    static final String BOOK_TITLE = "book-title";
    static final String ANNOTATION = "annotation";
    static final String KEYWORDS = "keywords";
    static final String DATE = "date";
    static final String LANG = "lang";
    static final String SRC_LANG = "src-lang";
    static final String TRANSLATOR = "translator";
    static final String SEQUENCE = "sequence";
    static final String IMAGE = "image";
    //DocumentInfo tags
    static final String PROGRAM_USED = "program-used";
    static final String SRC_URL = "src-url";
    static final String SRC_OCR = "src-ocr";
    static final String VERSION = "version";
    static final String HISTORY = "history";
    static final String PUBLISHER = "publisher";
    //person tags
    static final String FIRST_NAME = "first-name";
    static final String MIDDLE_NAME = "middle-name";
    static final String LAST_NAME = "last-name";
    static final String NICKNAME = "nickname";
    static final String HOME_PAGE = "home-page";
    static final String EMAIL = "email";
    //PublishInfo tags
    static final String BOOK_NAME = "book-name";
    static final String CITY = "city";
    static final String YEAR = "year";
    static final String ISBN = "isbn";
    //Sequence attribute
    static final String NAME = "name";
    static final String NUMBER = "number";
    //CoverPage attribute
    static final String HREF = "href";
    static final String TYPE = "type";
    static final String ALT = "alt";
    //binary tags
    static final String ELEMENT_BINARY = "binary";
    static final String CONTENT_TYPE = "content-type";

    static String getElementText(XMLStreamReader reader) throws XMLStreamException {
        final var elementText = reader.getElementText();
        if (!elementText.isBlank())
            return elementText;
        return null;
    }

    static String getDate(XMLStreamReader reader) throws XMLStreamException {
        if (reader.getAttributeCount() > 0)
            return reader.getAttributeValue(0);
        return getElementText(reader);
    }

}
