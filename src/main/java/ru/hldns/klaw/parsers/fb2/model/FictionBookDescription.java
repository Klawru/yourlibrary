package ru.hldns.klaw.parsers.fb2.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class FictionBookDescription {
    /**
     * Generic information about the book
     */
    protected TitleInfo titleInfo;
    /**
     * Generic information about the original book (for translations)
     */
    protected TitleInfo srcTitleInfo;
    /**
     * Information about this particular (xml) document
     */
    protected DocumentInfo documentInfo;
    /**
     * Information about some paper/outher published document, that was used as a source of this xml document
     */
    protected PublishInfo publishInfo;

    public boolean hasCoverPage() {
        return (titleInfo != null && titleInfo.hasCoverPage()) || (srcTitleInfo != null && srcTitleInfo.hasCoverPage());
    }

    public boolean hasCoverPage(String href) {
        return (titleInfo != null && titleInfo.hasCoverPage(href)) || (srcTitleInfo != null && srcTitleInfo.hasCoverPage(href));
    }

    public void addBinary(Binary binary) {
        if (titleInfo != null)
            titleInfo.addBinary(binary);
        if (srcTitleInfo != null)
            srcTitleInfo.addBinary(binary);
    }

    public boolean hasTitleInfo() {
        return titleInfo != null;
    }

    public boolean hasSrcTitleInfo() {
        return titleInfo != null;
    }
}
