package ru.hldns.klaw.parsers.fb2;

import com.fasterxml.aalto.WFCException;
import com.fasterxml.aalto.stax.InputFactoryImpl;
import com.fasterxml.aalto.util.IllegalCharHandler;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.context.MessageSource;
import ru.hldns.klaw.data.model.Book;
import ru.hldns.klaw.data.utility.MetaDataField;
import ru.hldns.klaw.parsers.fb2.file.FB2Parser;
import ru.hldns.klaw.parsers.fb2.file.StaxStreamProcessor;
import ru.hldns.klaw.parsers.fb2.model.FictionBookDescription;
import ru.hldns.klaw.parsers.fb2.model.ImageType;
import ru.hldns.klaw.parsers.fb2.model.PublishInfo;
import ru.hldns.klaw.parsers.fb2.model.TitleInfo;
import ru.hldns.klaw.service.FileService;
import ru.hldns.klaw.utility.Either;
import ru.hldns.klaw.web.service.CoverPageService;

import java.io.InputStream;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Optional;

@Slf4j
public class BookFileProcessor implements ItemProcessor<Book, Book> {

    private final FileService service;
    private final CoverPageService coverPageService;
    private final InputFactoryImpl inputFactory;
    private final ObjectWriter objectWriter;
    private final String untitled;

    public BookFileProcessor(FileService service, CoverPageService coverPageService, MessageSource messageSource) {
        this.service = service;
        this.coverPageService = coverPageService;

        //Setup STAX factory
        inputFactory = new InputFactoryImpl();
        final var config = inputFactory.getNonSharedConfig(null, null, null, false, false);
        config.setIllegalCharHandler(new IllegalCharHandler.ReplacingIllegalCharHandler(' '));

        //Setup json mapper
        final var objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        this.objectWriter = objectMapper.writerFor(FictionBookDescription.class);

        untitled = messageSource.getMessage("untitled", null, Locale.getDefault());
    }

    @Override
    public Book process(Book item) throws Exception {
        if (item.getDescription() == null) {
            final Optional<InputStream> fileStream = service.getInputStream(item.getFolder(), item.getFileName());
            if (fileStream.isPresent()) {
                try (StaxStreamProcessor processor = new StaxStreamProcessor(fileStream.get(), inputFactory)) {
                    //Парсим FB2 файл
                    final FictionBookDescription bookDescription = FB2Parser.parse(processor);
                    //Мапим на нужные поля
                    MapDescriptionOnBook(item, bookDescription);
                    //Отдаем если есть еще картинку
                    saveCoverPage(item, bookDescription);
                    return item;
                } catch (WFCException e) {
                    log.info("Ignored exception on parse file:{} with the message {}", item, e.getLocalizedMessage());
                }
            } else {
                log.warn("Return empty InputStream on Book: {}", item);
            }
        }
        return null;
    }

    private void saveCoverPage(Book item, FictionBookDescription bookDescription) {
        final Optional<ImageType> firstCoverPage = getFirstCoverPage(bookDescription);
        firstCoverPage.ifPresent(type -> {
            final var binary = type.getBinary();
            final Either<String, Exception> url =
                    coverPageService
                            .save(binary.getValue(), binary.getHash())
                            .ifPresent(item::setCoverPage);
        });
    }

    private Optional<ImageType> getFirstCoverPage(FictionBookDescription bookDescription) {
        ImageType imageType = null;
        if (bookDescription.hasTitleInfo()) {
            imageType = getImageType(bookDescription.getTitleInfo());
        }
        if (imageType == null && bookDescription.hasSrcTitleInfo()) {
            imageType = getImageType(bookDescription.getTitleInfo());
        }
        return Optional.ofNullable(imageType);
    }

    private ImageType getImageType(TitleInfo titleInfo) {
        final var images = titleInfo.getCoverPage();
        if (images != null && images.size() > 0) {
            for (final ImageType imageType : images) {
                if (imageType.getBinary() != null)
                    return imageType;
            }
        }
        return null;
    }

    private void MapDescriptionOnBook(Book book, FictionBookDescription description) {
        setPublishProperties(book, description.getPublishInfo());
        setTitleInfoProperties(book, description.getTitleInfo());
        book.setDescription(getDescriptionAsJson(description));
    }

    private void setTitleInfoProperties(Book book, TitleInfo titleInfo) {
        if (titleInfo != null) {
            setAnnotation(book, titleInfo);
            if (titleInfo.getBookTitle() != null && (book.getTitle().equals(untitled) || book.getTitle().isBlank()))
                book.setTitle(titleInfo.getBookTitle());
            if (titleInfo.getLang() != null && titleInfo.getLang().length() < 4 && book.getLang().isEmpty())
                book.setLang(titleInfo.getLang());
            if (book.getPublishedDate() == null) {
                final var date = titleInfo.getDate();
                if (date != null && !date.isBlank())
                    book.setPublishedDate(getYear(date));
            }
        }
    }

    private String getDescriptionAsJson(FictionBookDescription description) {
        try {
            return objectWriter.writeValueAsString(description);
        } catch (JsonProcessingException e) {
            log.error("Cannot convert FictionBookDescription class to json string: ", e);
        }
        return null;
    }

    private void setAnnotation(Book book, TitleInfo titleInfo) {
        final var annotation = titleInfo.getAnnotation();
        if (annotation != null) {
            book.setAnnotation(annotation);
        }

    }

    private void setPublishProperties(Book book, PublishInfo publishInfo) {
        if (publishInfo != null) {
            if (publishInfo.getYear() != null && !publishInfo.getYear().isBlank())
                book.setPublishedDate(getYear(publishInfo.getYear()));
            if (publishInfo.getIsbn() != null)
                book.addMetadata(MetaDataField.ISBN, publishInfo.getIsbn());
            if (publishInfo.getPublisher() != null)
                book.setPublisher(substring(publishInfo.getPublisher(), 250));
            if (publishInfo.getBookName() != null && (book.getTitle().equals(untitled) || book.getTitle().isBlank()))
                book.setTitle(substring(publishInfo.getBookName(), 250));
        }
    }

    @SuppressWarnings("SameParameterValue")
    static private String substring(String inputString, int max) {
        return inputString.length() < max ? inputString : inputString.substring(0, max) + "...";
    }


    private Year getYear(String date) {
        try {
            return Year.of(LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE).getYear());
        } catch (DateTimeException e) {
            return null;
        }
    }
}
