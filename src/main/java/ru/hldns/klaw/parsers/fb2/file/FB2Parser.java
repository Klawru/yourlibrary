package ru.hldns.klaw.parsers.fb2.file;

import com.fasterxml.aalto.WFCException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import ru.hldns.klaw.parsers.fb2.model.*;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.util.Arrays;
import java.util.Optional;

import static javax.xml.stream.events.XMLEvent.END_ELEMENT;
import static javax.xml.stream.events.XMLEvent.START_ELEMENT;
import static ru.hldns.klaw.parsers.fb2.file.FB2AnnotationParser.parseAnnotation;

@Slf4j
public class FB2Parser extends FB2AbstractParser {

    public static FictionBookDescription parse(StaxStreamProcessor processor) throws Exception {
        FictionBookDescription book = new FictionBookDescription();
        final XMLStreamReader reader = processor.getReader();
        try {
            while (reader.hasNext()) {
                int event = reader.next();
                if (event == START_ELEMENT) {
                    String tagName = reader.getLocalName();
                    if (DESCRIPTION.equals(tagName)) {
                        parseDescription(reader, book);
                        if (!book.hasCoverPage()) {
                            break;
                        }
                    } else if (ELEMENT_BINARY.equals(tagName)) {
                        parseBinary(reader, book).ifPresent(book::addBinary);
                    }
                }
            }
        } catch (WFCException e) {
            if (book.hasTitleInfo())
                return book;
            else
                throw e;
        }
        return book;
    }

    private static void parseDescription(final XMLStreamReader reader, FictionBookDescription description) throws XMLStreamException {
        while (reader.hasNext()) {       // while not end of XML
            int event = reader.next();   // read next event
            if (START_ELEMENT == event) {
                switch (reader.getLocalName()) {
                    case TITLE_INFO:
                        description.setTitleInfo(parseTitleInfo(reader, TITLE_INFO));
                        break;
                    case SRC_TITLE_INFO:
                        description.setSrcTitleInfo(parseTitleInfo(reader, SRC_TITLE_INFO));
                        break;
                    case DOCUMENT_INFO:
                        description.setDocumentInfo(parseDocumentInfo(reader));
                        break;
                    case PUBLISH_INFO:
                        description.setPublishInfo(parsePublishInfo(reader));
                        break;
                    case CUSTOM_INFO:
                        break;
                }
            } else if (END_ELEMENT == event) {
                if (DESCRIPTION.equals(reader.getLocalName())) {
                    break;
                }
            }
        }
    }


    private static TitleInfo parseTitleInfo(final XMLStreamReader reader, String endTag) throws XMLStreamException {
        TitleInfo documentInfo = new TitleInfo();
        while (reader.hasNext()) {
            int event = reader.next();
            if (event == START_ELEMENT) {
                switch (reader.getLocalName()) {
                    case GENRE:
                        documentInfo.addGenre(getElementText(reader));
                        break;
                    case AUTHOR:
                        documentInfo.addAuthor(parsePerson(reader, AUTHOR));
                        break;
                    case BOOK_TITLE:
                        documentInfo.setBookTitle(getElementText(reader));
                        break;
                    case ANNOTATION:
                        documentInfo.setAnnotation(parseAnnotation(reader, ANNOTATION));
                        break;
                    case KEYWORDS:
                        documentInfo.setKeywords(Arrays.asList(reader.getElementText().split(", ")));
                        break;
                    case DATE:
                        documentInfo.setDate(getDate(reader));
                        break;
                    case IMAGE:
                        documentInfo.addCoverPage(parseCoverPage(reader));
                        break;
                    case LANG:
                        documentInfo.setLang(getElementText(reader));
                        break;
                    case SRC_LANG:
                        documentInfo.setSrcLang(getElementText(reader));
                        break;
                    case TRANSLATOR:
                        documentInfo.addTranslator(parsePerson(reader, TRANSLATOR));
                        break;
                    case SEQUENCE:
                        documentInfo.addSequence(parseSequence(reader));
                        break;
                }
            } else if (event == END_ELEMENT && reader.getLocalName().equals(endTag))
                //end parse person
                break;
        }
        return documentInfo;
    }


    private static SeriesBook parseSequence(XMLStreamReader reader) {
        final SeriesBook seriesBook = new SeriesBook();
        for (int i = 0; i < reader.getAttributeCount(); i++)
            switch (reader.getAttributeLocalName(i)) {
                case NAME:
                    seriesBook.setName(reader.getAttributeValue(i));
                    break;
                case NUMBER:
                    seriesBook.setNumber(reader.getAttributeValue(i));
                case LANG:
                    seriesBook.setLang(reader.getAttributeValue(i));
            }
        return seriesBook;
    }

    private static ImageType parseCoverPage(XMLStreamReader reader) {
        final ImageType imageType = new ImageType();
        for (int i = 0; i < reader.getAttributeCount(); i++)
            switch (reader.getAttributeLocalName(i)) {
                case HREF:
                    imageType.setHref(reader.getAttributeValue(i).substring(1));
                    break;
                case TYPE:
                    imageType.setType(reader.getAttributeValue(i));
                    break;
                case ALT:
                    imageType.setAlt(reader.getAttributeValue(i));
                    break;
            }
        return imageType;
    }

    private static DocumentInfo parseDocumentInfo(XMLStreamReader reader) throws XMLStreamException {
        DocumentInfo documentInfo = new DocumentInfo();
        while (reader.hasNext()) {
            int event = reader.next();
            if (event == START_ELEMENT) {
                switch (reader.getLocalName()) {
                    case AUTHOR:
                        documentInfo.addAuthor(parsePerson(reader, AUTHOR));
                        break;
                    case PROGRAM_USED:
                        documentInfo.setProgramUsed(getElementText(reader));
                        break;
                    case DATE:
                        documentInfo.setDate(getDate(reader));
                        break;
                    case SRC_URL:
                        documentInfo.addSrcUrl(getElementText(reader));
                        break;
                    case SRC_OCR:
                        documentInfo.setSrcOcr(getElementText(reader));
                        break;
                    case ELEMENT_ID:
                        documentInfo.setId(getElementText(reader));
                        break;
                    case VERSION:
                        documentInfo.setVersion(getElementText(reader));
                        break;
                    case HISTORY:
                        documentInfo.setHistory(parseAnnotation(reader, HISTORY));
                        break;
                    case PUBLISHER:
                        documentInfo.addPublisher(parsePerson(reader, PUBLISHER));
                        break;
                }
            } else if (event == END_ELEMENT && DOCUMENT_INFO.equals(reader.getLocalName()))
                //end parse
                break;
        }
        return documentInfo;
    }

    private static PublishInfo parsePublishInfo(XMLStreamReader reader) throws XMLStreamException {
        PublishInfo publishInfo = new PublishInfo();
        while (reader.hasNext()) {
            int event = reader.next();
            if (event == START_ELEMENT) {
                switch (reader.getLocalName()) {
                    case BOOK_NAME:
                        publishInfo.setBookName(getElementText(reader));
                        break;
                    case PUBLISHER:
                        publishInfo.setPublisher(getElementText(reader));
                        break;
                    case CITY:
                        publishInfo.setCity(getElementText(reader));
                        break;
                    case YEAR:
                        publishInfo.setYear(getElementText(reader));
                        break;
                    case ISBN:
                        publishInfo.setIsbn(getElementText(reader));
                        break;
                    case SEQUENCE:
                        publishInfo.addSequence(parseSequence(reader));
                        break;
                }
            } else if (event == END_ELEMENT && PUBLISH_INFO.equals(reader.getLocalName()))
                //end parse
                break;
        }
        return publishInfo;
    }

    private static Person parsePerson(XMLStreamReader reader, String endTag) throws XMLStreamException {
        Person person = new Person();
        while (reader.hasNext()) {
            int event = reader.next();
            if (event == START_ELEMENT) {
                switch (reader.getLocalName()) {
                    case FIRST_NAME:
                        person.setFirstName(getElementText(reader));
                        break;
                    case MIDDLE_NAME:
                        person.setMiddleName(getElementText(reader));
                        break;
                    case LAST_NAME:
                        person.setLastName(getElementText(reader));
                        break;
                    case NICKNAME:
                        person.setNickname(getElementText(reader));
                        break;
                    case HOME_PAGE:
                        person.addHomePage(getElementText(reader));
                        break;
                    case EMAIL:
                        person.addEmail(getElementText(reader));
                        break;
                    case ELEMENT_ID:
                        person.setId(getElementText(reader));
                        break;
                }
            } else if (event == END_ELEMENT && reader.getLocalName().equals(endTag))
                break;
        }
        return person;
    }

    private static Optional<Binary> parseBinary(XMLStreamReader reader, FictionBookDescription book) {
        Binary binary = new Binary();
        for (int i = 0; i < reader.getAttributeCount(); i++)
            switch (reader.getAttributeLocalName(i)) {
                case CONTENT_TYPE:
                    binary.setContentType(reader.getAttributeValue(i));
                    break;
                case ELEMENT_ID:
                    binary.setId(reader.getAttributeValue(i));
                    break;
            }
        if (!book.hasCoverPage(binary.getId()))
            return Optional.empty();
        try {
            final String base64 = reader.getElementText();
            binary.setValue(Base64.decodeBase64(base64));
            binary.setHash(DigestUtils.md5Hex(binary.getValue()).toUpperCase());
            book.addBinary(binary);
        } catch (Exception e) {
            return Optional.empty();
        }
        binary.setContentType(getContentType(binary));

        return Optional.of(binary);
    }

    private static String getContentType(Binary binary) {
        if (binary.getContentType() == null ||
                !(         binary.getContentType().equals("image/jpg")
                        || binary.getContentType().equals("image/jpeg")
                        || binary.getContentType().equals("image/png"))) {
            if (binary.getId().endsWith("png"))
                return "image/png";
            if (binary.getId().endsWith("jpeg") || binary.getId().endsWith("jpg"))
                return "image/jpg";
            return ("image/jpeg");
        }
        return binary.getContentType();
    }

}