package ru.hldns.klaw.parsers.fb2.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class DocumentInfo {
    /**
     * Author(s) of this particular document
     */
    protected List<Person> author;

    /**
     * Date this document was created, same guidelines as in the <title-info> section apply
     */
    protected String date;
    /**
     * Source URL if this document is a conversion of some other (online) document
     */
    protected List<String> srcUrl;
    /**
     * Author of the original (online) document, if this is a conversion
     */
    protected String srcOcr;
    /**
     * this is a unique identifier for a document. this must not change
     */
    protected String id;

    /**
     * Document version, in free format, should be incremented if the document is changed and re-released to the public
     */
    protected String version;

    /**
     * Short description for all changes made to this document, like \"Added missing chapter 6\", in free form.
     */
    protected String history;

    /**
     * Owner of the fb2 document copyrights
     */
    protected List<Person> publisher;

    /**
     * Any software used in preparation of this document, in free format
     */
    private String programUsed;

    public void addAuthor(Person parsePerson) {
        if (author == null)
            author = new ArrayList<>();
        author.add(parsePerson);
    }

    public void addSrcUrl(String elementText) {
        if (srcUrl == null)
            srcUrl = new ArrayList<>();
        srcUrl.add(elementText);
    }

    public void addPublisher(Person parsePerson) {
        if (publisher == null)
            publisher = new ArrayList<>();
        publisher.add(parsePerson);
    }
}
