package ru.hldns.klaw.parsers;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Primary;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.stereotype.Component;
import ru.hldns.klaw.utility.CacheEvictEvent;

@Primary
@Component
public class JobListener implements JobExecutionListener {

    private final ApplicationEventPublisher publisher;

    @Autowired
    protected JobListener(ApplicationEventPublisher publisher) {
        this.publisher = publisher;
    }

    @Override
    public void beforeJob(JobExecution jobExecution) {
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        try {
            publisher.publishEvent(new CacheEvictEvent(this));
        } catch (JpaSystemException ignore) {
            //could not extract ResultSet
        }
    }

}
