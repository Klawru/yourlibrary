package ru.hldns.klaw.parsers.inpx.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ParsedFile implements Serializable {
    @Id
    @Column(unique = true)
    private String name;

    private long hash;

    private long rows;

    private boolean isComplete;
}
