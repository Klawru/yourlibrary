package ru.hldns.klaw.parsers.inpx.model;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.hldns.klaw.data.model.*;
import ru.hldns.klaw.data.repository.*;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ParserServiceDAO {

    private final GenreRepository genreRepository;
    private final CommunityRepository communityRepository;
    private final AuthorsRepository authorsRepository;
    private final BookRepository bookRepository;
    private final ParsedFileRepository parsedFileRepository;
    private final BookSeriesRepository seriesRepository;

    @Autowired
    public ParserServiceDAO(GenreRepository genreRepository, CommunityRepository communityRepository,
                            AuthorsRepository authorsRepository, BookRepository bookRepository,
                            ParsedFileRepository parsedFileRepository, BookSeriesRepository bookSeriesRepository) {
        this.genreRepository = genreRepository;
        this.communityRepository = communityRepository;
        this.authorsRepository = authorsRepository;
        this.bookRepository = bookRepository;
        this.parsedFileRepository = parsedFileRepository;
        this.seriesRepository = bookSeriesRepository;
    }


    public Author saveAuthor(@NonNull Author author) {
        return authorsRepository.save(author);
    }

    public Optional<Author> findAuthor(@NonNull String author) {
        return authorsRepository.findByFullNameIs(author);
    }

    public Genre saveGenre(@NonNull Genre genre) {
        return genreRepository.save(genre);
    }

    public Optional<Genre> findGenre(@NonNull Genre genre) {
        return genreRepository.findByGenreIs(genre.getGenre());
    }

    public Optional<Series> findBookSeries(@NonNull Series series) {
        return seriesRepository.findByNameIs(series.name);
    }

    public Series saveBookSeries(@NonNull Series series) {
        return seriesRepository.save(series);
    }

    public List<Genre> getAllGenres() {
        return genreRepository.findAll();

    }

    public Optional<Community> findCommunityById(int id) {
        return communityRepository.findById(id);
    }

    public void saveItems(Iterable<? extends Book> book) {
        bookRepository.saveAll(book);
    }

    public void deleteAllParsedFile() {
        parsedFileRepository.deleteAll();
    }

    public List<ParsedFile> findAllParsedFile() {
        return parsedFileRepository.findAll();
    }

    public void saveParsedFiles(Iterable<ParsedFile> parsedFiles) {
        parsedFileRepository.saveAll(parsedFiles);
    }

    public void saveItem(Book first) {
        bookRepository.save(first);
    }
}
