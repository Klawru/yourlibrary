package ru.hldns.klaw.service;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.github.benmanes.caffeine.cache.RemovalCause;
import com.github.benmanes.caffeine.cache.RemovalListener;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;
import ru.hldns.klaw.config.ApplicationConfig;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.Optional;
import java.util.zip.ZipFile;

@Slf4j
@Service
public class FileService implements RemovalListener<String, ZipFile> {

    private final String uploadingDir;
    private final LoadingCache<String, ZipFile> fileCache;

    public FileService(ApplicationConfig applicationConfig) {
        uploadingDir = applicationConfig.getLibraryDir();
        fileCache = Caffeine.newBuilder()
                .expireAfterAccess(Duration.ofMinutes(5))
                .removalListener(this)
                .recordStats()
                .build(this::getZipFile);
    }

    public Optional<StreamingResponseBody> getFileStream(final String folder, final String fileName) {
        try {
            final var zipFile = fileCache.get(folder);
            if (zipFile != null) {
                final var entry = zipFile.getEntry(fileName);
                return Optional.of(
                        (OutputStream out) -> {
                            try (var inputStream = zipFile.getInputStream(entry)) {
                                inputStream.transferTo(out);
                            } catch (final IOException e) {
                                log.error("Exception while reading and streaming model", e);
                            }
                        }
                );
            } else
                log.error("File not found with folder:'{}', filename:'{}', cache return null", folder, fileName);
        } catch (IllegalStateException e) {
            log.error("Exception while get Entry", e);
        }
        return Optional.empty();
    }

    public Optional<InputStream> getInputStream(final String folder, final String fileName) {
        try {
            final var zipFile = fileCache.get(folder);
            if (zipFile != null) {
                final var entry = zipFile.getEntry(fileName);
                return Optional.of(zipFile.getInputStream(entry));
            } else
                log.error("File not found with folder:'{}', filename:'{}', cache return null", folder, fileName);
        } catch (IllegalStateException | IOException e) {
            log.error("Exception while get Entry", e);
        }
        return Optional.empty();
    }

    private ZipFile getZipFile(String filename) throws IOException {
        final var path = Paths.get(uploadingDir, filename + ".zip");
        final var file = path.toFile();
        if (file.exists())
            return new ZipFile(file);
        else
            throw new IOException("Not found file");
    }

    @Override
    public void onRemoval(@Nullable String s, @Nullable ZipFile zipFile, @NonNull RemovalCause removalCause) {
        if (zipFile != null)
            try {
                zipFile.close();
            } catch (IOException e) {
                log.error("Error on close zipFile", e);
            }
    }
}
