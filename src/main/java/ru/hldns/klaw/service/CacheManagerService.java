package ru.hldns.klaw.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import ru.hldns.klaw.utility.CacheEvictEvent;

@Service
@Slf4j
public class CacheManagerService {
    private final CacheManager cacheManager;

    @Autowired
    public CacheManagerService(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    public void evictAllCaches() {
        cacheManager.getCacheNames().parallelStream().forEach(this::evictCache);
        log.info("Evict all caches");
    }

    public void evictCache(String name) {
        final var cache = cacheManager.getCache(name);
        if (cache != null) {
            cache.clear();
            log.trace("Evict cache by name \"{}\"",name);
        }
    }

    @EventListener
    public void handleEvent(CacheEvictEvent cacheEvictEvent){
        evictAllCaches();
    }
}
