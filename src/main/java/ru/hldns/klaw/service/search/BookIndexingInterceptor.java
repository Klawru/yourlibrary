package ru.hldns.klaw.service.search;

import org.hibernate.search.indexes.interceptor.EntityIndexingInterceptor;
import org.hibernate.search.indexes.interceptor.IndexingOverride;
import ru.hldns.klaw.data.model.Book;

public class BookIndexingInterceptor implements EntityIndexingInterceptor<Book> {
    @Override
    public IndexingOverride onAdd(Book entity) {
        if (entity.isDeleted()) {
            return IndexingOverride.SKIP;
        }
        return IndexingOverride.APPLY_DEFAULT;
    }

    @Override
    public IndexingOverride onUpdate(Book entity) {
        if (entity.isDeleted()) {
            return IndexingOverride.REMOVE;
        }
        return IndexingOverride.APPLY_DEFAULT;
    }

    @Override
    public IndexingOverride onDelete(Book entity) {
        return IndexingOverride.APPLY_DEFAULT;
    }

    @Override
    public IndexingOverride onCollectionUpdate(Book entity) {
        return onUpdate(entity);
    }
}
