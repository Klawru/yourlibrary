FROM maven:3.6.3-openjdk-14-slim

WORKDIR /usr/src/app

COPY . /usr/src/app
RUN mvn package

ENV PORT 8080
EXPOSE $PORT
CMD [ "sh", "-c", "mvn compile" ]
CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} -DskipTests=true spring-boot:run" ]
