### Yoyr Library Project
### Introduction
Проект работает с архивами [Флибуста](https://ru.wikipedia.org/wiki/%D0%A4%D0%BB%D0%B8%D0%B1%D1%83%D1%81%D1%82%D0%B0) в формате `.inpx`, и предостваляет интерфейс для поиска книг по архиву.

The project works with [Flibust](https://ru.wikipedia.org/wiki/%D0%A4%D0%BB%D0%B8%D0%B1%D1%83%D1%81%D1%82%D0%B0) archives in the `.inpx` format, and provides an interface for searching for books in the archive.

![Главная_страница_Your_Library](https://gitlab.com/Klawru/yourlibrary/uploads/d0a76bfbfd712875f82a1f506140bb3d/Screenshot_2019-12-01_%D0%93%D0%BB%D0%B0%D0%B2%D0%BD%D0%B0%D1%8F_%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D0%B0_Your_Library.png)

Database configuration

With docker:
```
docker run --name PostgresDB -e POSTGRES_PASSWORD=your_library -e POSTGRES_DB=your_library -d postgres
```
